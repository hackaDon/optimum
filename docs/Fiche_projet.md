# Fiche_projet
## optiDON

Le projet d'optiDON est de créer une plateforme vidéo avec régis publicitaire intégré recensant les publicités de différents secteurs en catégorie, qui, en étant regardé par des utilisateurs, permettra de générer des dons de la part des marques en question.

**Pour l'utilisateur : don de temps
Pour la marque : don d'argent**


## optiDON, c'est quoi ?

Le projet d'optiDON est de créer une plateforme vidéo avec régie publicitaire intégrée recensant les publicités de différents secteurs en catégorie, qui en étant regardé par des utilisateurs permettra de générer des dons de la part des marques en question.

Sur la plateforme, les différentes marques seront catégorisées selon leurs secteurs (automobile, mode, sport, agroalimentaire...), les utilisateurs regarderont alors les marques qui correspondent à leurs centres d'intérêts. Une fois les publicités regardées, un don sera fait par la marque en question à une association,
à condition que l'utilisateur s'inscrive sur la plateforme. Il aura la possibilité de partager son action sur les différents réseaux sociaux (Facebook, Twitter et Linkedin) afin de multiplier les gains de la vidéo.
Un système de "gamification" (car très à la mode dû au fait que les utilisateurs aiment le challenge) sera rajouté. L'utilisateur ayant vue le plus de publicités à la fin du jour/semaine/mois se verra remettre un bon de réduction d'une certaine valeur (exemple : 15%) par la marque qu'il aura le plus visionné.
Afin de rassurer les marques sur le fait que leurs publicités seront bien regardées, des QR codes (permettant d'avoir 5% de réduction chez la marque en question) apparaitront au hasard dans certaines publicités, ce qui obligera l'utilisateur à rester attentif au contenu.

 - Quoi ?

Plateforme recensant les publicités de différents secteurs sous forme de catégories, qui en étant regardées par des utilisateurs permettra de générer des dons de la part des marques en question.

 - Pourquoi ?
 
Les jeunes d'aujourd'hui ont en général plus de temps à donner que d'argent, ici ils seront appelés à donner de leur temps (du temps dépensé qui leur sera bénéfique grâce aux codes promos), alors que les entreprises feront des dons d'argent.
 
 - A qui ?

Aux jeunes ne pouvant pas participer financièrement pour des associations.

 - Valeurs ajoutées ?

	- Gamification
	- QR codes
	- Catégories (Produits français, produits écologiques...)


## Lien de la maquette

https://xd.adobe.com/view/79c2cf22-b41a-46e5-68b6-813087368f6c-0ff3/?fullscreen
