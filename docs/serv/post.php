
<?php   
$url = 'https://blocksy-test.symag.com/symag2/blocksy/api/v1/identity/entity';

  $data = array(
    "client_id" => "1234",
    "spec_version" => 1,
    "created_by" => "blocksy",
    "name" => "testwork",
    "type_uid" => "237a2682-f82e-46af-b7d5-a3450f1cee30",
    "type_version" => 1,
    "status" => 1
  );
$data_json = json_encode($data);

  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json'
  ));

  $server_output = curl_exec ($ch);

  echo  $server_output;
